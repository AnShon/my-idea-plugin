package com.cas.plugin;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.logging.Logger;

/**
 * <p>Description: AnAction 生成文件 </p>
 *
 * @author chenanshun
 * @date 2018/11/13 13:58
 */
public class CreateClassAction extends AnAction {
    //    public CreateClassAction() {
//        // 设置菜单项名称
//        super("CreateClassAction");
//        // 设置菜单项名称、描述和图标
//        // super("Text _Boxes","Item description",IconLoader.getIcon("/Mypackage/icon.png"));
//    }
    private static Logger logger = Logger.getLogger(CreateClassAction.class.getCanonicalName());

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(PlatformDataKeys.PROJECT);
        // 获取表名
        String tableName = Messages.showInputDialog(project, "Table Name:", "Input A Table Name", Messages.getQuestionIcon());
        if (StringUtils.isBlank(tableName)) {
            Messages.showErrorDialog(project, "Table Name Is Null", "Error");
        } else {
//            Messages.showErrorDialog(project, TemplateUtils.templatePath(), "Error");
            // 获取数据库配置
            SettingComponent setting = SettingComponent.getInstance();
            setting.setTableName(tableName);
            // 获取项目root路径
            VirtualFile virtualFile = event.getData(PlatformDataKeys.VIRTUAL_FILE);
            VirtualFile moduleContentRoot = ProjectRootManager.getInstance(project).getFileIndex().getContentRootForFile(virtualFile);
            setting.setDestFilePath(moduleContentRoot.getPath() + File.separator + setting.packagePathToFilePath(setting.getSrcFolder()));
            try {
                if (org.apache.commons.lang3.StringUtils.isBlank(setting.getServicePackagePath())
                        || org.apache.commons.lang3.StringUtils.isBlank(setting.getMapperPackagePath())
                        || org.apache.commons.lang3.StringUtils.isBlank(setting.getControllerPackagePath())) {
                    logger.warning("未设置包路径\n");
                } else {
                    GeneratorCode.main();// 生成文件
                }
            } catch (Exception e) {
                logger.warning("生成文件异常\n" + e.getMessage());
            }
        }
    }
}

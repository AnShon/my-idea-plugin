package com.cas.plugin;

import javax.swing.*;

/**
 * <p>Description: </p>
 *
 * @author chenanshun
 * @date 2018/11/13 15:13
 */
public class settingForm {
    JPanel configJpanel;
    JTabbedPane jdbcTabbedPane;
    JTextField urlTextField;
    JTextField userNameTextField;
    JTextField passwordTextField;

    JTextField srcFolderTextField;
    JTextField modelPackagePathTextField;
    JTextField mapperPackagePathTextField;
    JTextField servicePackagePathTextField;
    JTextField controllerPackagePathTextField;
    JTextField autherTextField;
    JTextArea controller;
    JTextArea service;
    JTextArea mapper;
    JRadioButton saveToPackagePathRadioButton;
}

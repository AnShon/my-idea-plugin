package com.cas.plugin;

import com.cas.plugin.util.TemplateUtils;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * <p>Description: 参数配置 </p>
 *
 * @author chenanshun
 * @date 2018/11/13 14:32
 */
public class SettingConfigurable implements SearchableConfigurable {

    private static Logger logger = Logger.getLogger(SettingConfigurable.class.getCanonicalName());
    private settingForm configForm;
    private SettingComponent setting = SettingComponent.getInstance();

    @NotNull
    @Override
    public String getId() {
        return "AnShon Ggenerator Code";
    }

    @Nls
    @Override
    public String getDisplayName() {
        return this.getId();
    }

    @Nullable
    @Override
    public JComponent createComponent() {// 绘制插件参数配置界面
        if (null == this.configForm) {
            this.configForm = new settingForm();
        }
        return this.configForm.configJpanel;
    }

    @Override
    public boolean isModified() {
        return !this.configForm.urlTextField.getText().equals(this.setting.getUrl())
                || !this.configForm.userNameTextField.getText().equals(this.setting.getUserName())
                || !this.configForm.passwordTextField.getText().equals(this.setting.getPassword())
                || !this.configForm.passwordTextField.getText().equals(this.setting.getPassword())

                || !this.configForm.srcFolderTextField.getText().equals(this.setting.getSrcFolder())
                || !this.configForm.modelPackagePathTextField.getText().equals(this.setting.getModelPackagePath())
                || !this.configForm.mapperPackagePathTextField.getText().equals(this.setting.getMapperPackagePath())
                || !this.configForm.servicePackagePathTextField.getText().equals(this.setting.getServicePackagePath())
                || !this.configForm.controllerPackagePathTextField.getText().equals(this.setting.getControllerPackagePath())
                || !this.configForm.autherTextField.getText().equals(this.setting.getAuther())

                || !this.configForm.controller.getText().equals(this.setting.getController())
                || !this.configForm.service.getText().equals(this.setting.getService())
                || !this.configForm.mapper.getText().equals(this.setting.getMapper())
                || this.configForm.saveToPackagePathRadioButton.isSelected() != this.setting.isSaveToPackage()
                ;
    }

    @Override
    public void apply() throws ConfigurationException {
        this.setting.setPassword(this.configForm.passwordTextField.getText());
        this.setting.setUrl(this.configForm.urlTextField.getText());
        this.setting.setUserName(this.configForm.userNameTextField.getText());

        this.setting.setModelPackagePath(this.configForm.modelPackagePathTextField.getText());
        this.setting.setMapperPackagePath(this.configForm.mapperPackagePathTextField.getText());
        this.setting.setServicePackagePath(this.configForm.servicePackagePathTextField.getText());
        this.setting.setControllerPackagePath(this.configForm.controllerPackagePathTextField.getText());
        this.setting.setSrcFolder(this.configForm.srcFolderTextField.getText());
        this.setting.setAuther(this.configForm.autherTextField.getText());

        this.setting.setSaveToPackage(this.configForm.saveToPackagePathRadioButton.isSelected());

        this.setting.setModelClassFilePath(this.setting.packagePathToFilePath(this.setting.getModelPackagePath()));
        this.setting.setMapperClassFilePath(this.setting.packagePathToFilePath(this.setting.getMapperPackagePath()));
        this.setting.setServiceClassFilePath(this.setting.packagePathToFilePath(this.setting.getServicePackagePath()));
        this.setting.setControllerClassFilePath(this.setting.packagePathToFilePath(this.setting.getControllerPackagePath()));


        this.setting.setService(this.configForm.service.getText());
        this.setting.setMapper(this.configForm.mapper.getText());
        this.setting.setController(this.configForm.controller.getText());
        this.createTemplateFile();
    }

    public void reset() {
        if (StringUtils.isNotBlank(this.setting.getUrl()))
            this.configForm.urlTextField.setText(this.setting.getUrl());
        this.configForm.passwordTextField.setText(this.setting.getPassword());
        this.configForm.userNameTextField.setText(this.setting.getUserName());
        this.configForm.modelPackagePathTextField.setText(this.setting.getModelPackagePath());
        this.configForm.mapperPackagePathTextField.setText(this.setting.getMapperPackagePath());
        this.configForm.servicePackagePathTextField.setText(this.setting.getServicePackagePath());
        this.configForm.controllerPackagePathTextField.setText(this.setting.getControllerPackagePath());
        this.configForm.saveToPackagePathRadioButton.setSelected(this.setting.isSaveToPackage());
        if (StringUtils.isNotBlank(this.setting.getSrcFolder()))
            this.configForm.srcFolderTextField.setText(this.setting.getSrcFolder());
        this.configForm.autherTextField.setText(this.setting.getAuther());

        this.setting.setModelClassFilePath(this.setting.packagePathToFilePath(this.setting.getModelPackagePath()));
        this.setting.setMapperClassFilePath(this.setting.packagePathToFilePath(this.setting.getMapperPackagePath()));
        this.setting.setServiceClassFilePath(this.setting.packagePathToFilePath(this.setting.getServicePackagePath()));
        this.setting.setControllerClassFilePath(this.setting.packagePathToFilePath(this.setting.getControllerPackagePath()));
        this.createTemplateFile();
    }

    // 异步生成模板文件
    public void createTemplateFile() {
        new Thread(() -> {
            try {
                String url = TemplateUtils.templatePath();
                String controllersUrl = url + File.separator + TemplateUtils.TEMPLATE_FILE_NAME_CONTROLLER;
                String mapperUrl = url + File.separator + TemplateUtils.TEMPLATE_FILE_NAME_MAPPER;
                String serviceUrl = url + File.separator + TemplateUtils.TEMPLATE_FILE_NAME_SERVICE;
                if (StringUtils.isNotBlank(this.setting.getController())) {
                    this.configForm.controller.setText(this.setting.getController());
                    Files.write(Paths.get(controllersUrl), this.setting.getController().getBytes());
                } else {
                    Files.write(Paths.get(controllersUrl), this.configForm.controller.getText().getBytes());
                }

                if (StringUtils.isNotBlank(this.setting.getMapper())) {
                    this.configForm.mapper.setText(this.setting.getMapper());
                    Files.write(Paths.get(mapperUrl), this.setting.getMapper().getBytes());
                } else {
                    Files.write(Paths.get(mapperUrl), this.configForm.mapper.getText().getBytes());
                }

                if (StringUtils.isNotBlank(this.setting.getService())) {
                    this.configForm.service.setText(this.setting.getService());
                    Files.write(Paths.get(serviceUrl), this.setting.getService().getBytes());
                } else {
                    Files.write(Paths.get(serviceUrl), this.configForm.service.getText().getBytes());
                }
            } catch (Exception e) {
                logger.warning("异步生成模板文件异常\n" + e.getMessage());
            }
        }).start();
    }
}

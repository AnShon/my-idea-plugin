package com.cas.plugin.common;

public interface Template {
    String CONTROLLER = "${controllerPackage};\n" +
            "import io.swagger.annotations.Api;\n" +
            "import io.swagger.annotations.ApiOperation;\n" +
            "import lombok.extern.log4j.Log4j2;\n" +
            "import org.apache.commons.lang.StringUtils;\n" +
            "import org.apache.commons.lang.time.StopWatch;\n" +
            "import org.hibernate.validator.constraints.NotBlank;\n" +
            "import org.springframework.beans.factory.annotation.Autowired;\n" +
            "import org.springframework.data.redis.core.StringRedisTemplate;\n" +
            "import org.springframework.http.HttpHeaders;\n" +
            "import org.springframework.http.HttpStatus;\n" +
            "import org.springframework.http.ResponseEntity;\n" +
            "import org.springframework.validation.annotation.Validated;\n" +
            "import org.springframework.web.bind.annotation.CrossOrigin;\n" +
            "import org.springframework.web.bind.annotation.GetMapping;\n" +
            "import org.springframework.web.bind.annotation.RequestHeader;\n" +
            "import org.springframework.web.bind.annotation.RequestMapping;\n" +
            "import org.springframework.web.bind.annotation.RequestParam;\n" +
            "import org.springframework.web.bind.annotation.RestController;\n" +
            "import ${serviceClassPath}.${fileName}Service;\n" +
            "import com.goodsogood.ows.component.Errors;\n" +
            "\n" +
            "/**\n" +
            " *\n" +
            " * @author ${auther}\n" +
            " * @date ${time}\n" +
            " **/\n" +
            "@RestController\n" +
            "@RequestMapping(\"\")\n" +
            "@Log4j2\n" +
            "@CrossOrigin(origins = \"*\", maxAge = 3600)\n" +
            "@Api(value = \"\", tags = {\"\"})\n" +
            "@Validated\n" +
            "public class ${fileName}Controller {\n" +
            "\n" +
            "     private final Errors errors;\n" +
            "     private final ${fileName}Service ${service}Service;\n" +
            "\n" +
            "     @Autowired\n" +
            "     public ${fileName}Controller(Errors errors, ${fileName}Service ${service}Service) {\n" +
            "          this.errors = errors;\n" +
            "          this.${service}Service = ${service}Service;\n" +
            "     }\n" +
            "\n" +
            "}\n";



    String MAPPER = "${mapperPackage};\n" +
            "\n" +
            "import ${modelClassPath}.${fileName}Entity;\n" +
            "import org.apache.ibatis.annotations.*;\n" +
            "import org.springframework.stereotype.Repository;\n" +
            "\n" +
            "/**\n" +
            " * @author ${auther}\n" +
            " * @date ${time}\n" +
            " **/\n" +
            "@Repository\n" +
            "@Mapper\n" +
            "public interface ${fileName}Mapper extends MyMapper<${fileName}Entity>{\n" +
            "\n" +
            "}";

    String SERVICE = "${servicePackage};\n" +
            "\n" +
            "import lombok.extern.log4j.Log4j2;\n" +
            "import org.elasticsearch.common.Strings;\n" +
            "import org.springframework.beans.factory.annotation.Autowired;\n" +
            "import org.springframework.beans.factory.annotation.Value;\n" +
            "import org.springframework.scheduling.annotation.Async;\n" +
            "import org.springframework.stereotype.Service;\n" +
            "import tk.mybatis.mapper.entity.Example;\n" +
            "\n" +
            "import ${mapperClassPath}.${fileName}Mapper;\n" +
            "\n" +
            "/**\n" +
            " * @author ${auther}\n" +
            " * @date ${time}\n" +
            " **/\n" +
            "@Service\n" +
            "@Log4j2\n" +
            "public class ${fileName}Service {\n" +
            "\n" +
            "    private final ${fileName}Mapper ${mapper}Mapper;\n" +
            "\n" +
            "    @Autowired\n" +
            "    public ${fileName}Service(${fileName}Mapper ${mapper}Mapper) {\n" +
            "        this.${mapper}Mapper = ${mapper}Mapper;\n" +
            "    }\n" +
            "\n" +
            "}";
}

package com.cas.plugin;

import com.cas.plugin.util.TemplateUtils;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jdom.Element;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * <p>Description: 持久化配置 </p>
 *
 * @author chenanshun
 * @date 2018/11/13 14:48
 */
@State(
        name = "SettingComponent",
        storages = {@Storage(
//                id = "other",
                file = "$APP_CONFIG$/SettingComponent.xml"
        )}
)
public class SettingComponent implements PersistentStateComponent<Element> {
    private String auther;
    private String fileName;
    /**
     * 生成文件的地址
     */
    private String destFileDefaultPath = "D://generatore-code";
    private String destFilePath;
    /**
     * 类文件生成的地址
     */
    private String modelClassFilePath;
    private String controllerClassFilePath;
    private String serviceClassFilePath;
    private String mapperClassFilePath;

    private String srcFolder;
    /**
     * model包地址
     */
    private String modelPackagePath;
    /**
     * controller的包地址
     */
    private String controllerPackagePath;
    /**
     * service的包地址
     */
    private String servicePackagePath;
    /**
     * mapper的包地址
     */
    private String mapperPackagePath;

    private String tableName;

    private String url;
    private String userName;
    private String password;


    // 模版
    private String controller;
    private String mapper;
    private String service;

    private boolean saveToPackage=false;

    private SettingComponent() {
    }

    public static SettingComponent getInstance() {
        return ServiceManager.getService(SettingComponent.class);//服务管理可以获取所有
    }


    @Nullable
    @Override
    public Element getState() {
        Element element = new Element("SettingComponent");
        element.setAttribute("url", this.getUrl());
        element.setAttribute("userName", this.getUserName());
        element.setAttribute("password", this.getPassword());

        element.setAttribute("modelPackagePath", this.getModelPackagePath());
        element.setAttribute("mapperPackagePath", this.getMapperPackagePath());
        element.setAttribute("servicePackagePath", this.getServicePackagePath());
        element.setAttribute("controllerPackagePath", this.getControllerPackagePath());
        element.setAttribute("srcFolder", this.getSrcFolder());
        element.setAttribute("auther", this.getAuther());

        element.setAttribute("controller", this.getController());
        element.setAttribute("mapper", this.getMapper());
        element.setAttribute("service", this.getService());
        element.setAttribute("saveToPackage", this.isSaveToPackage()+"");


        return element;
    }

    @Override
    public void loadState(@NotNull Element element) {
        this.setUserName(element.getAttributeValue("userName"));
        this.setUrl(element.getAttributeValue("url"));
        this.setPassword(element.getAttributeValue("password"));


        this.setMapper(element.getAttributeValue("mapper"));
        this.setService(element.getAttributeValue("service"));
        this.setController(element.getAttributeValue("controller"));

        this.setModelPackagePath(element.getAttributeValue("modelPackagePath"));
        this.setMapperPackagePath(element.getAttributeValue("mapperPackagePath"));
        this.setServicePackagePath(element.getAttributeValue("servicePackagePath"));
        this.setControllerPackagePath(element.getAttributeValue("controllerPackagePath"));
        this.setSrcFolder(element.getAttributeValue("srcFolder"));
        this.setAuther(element.getAttributeValue("auther"));

        this.setSaveToPackage(Boolean.valueOf(element.getAttributeValue("saveToPackage")));

        this.setModelClassFilePath(this.packagePathToFilePath(this.getModelPackagePath()));
        this.setMapperClassFilePath(this.packagePathToFilePath(this.getMapperPackagePath()));
        this.setServiceClassFilePath(this.packagePathToFilePath(this.getServicePackagePath()));
        this.setControllerClassFilePath(this.packagePathToFilePath(this.getControllerPackagePath()));

        TemplateUtils.initTemplateFile();
    }

    public String packagePathToFilePath(String packagePath) {
        if (StringUtils.isBlank(packagePath))
            return "";
        return StringUtils.join(packagePath.split("\\."), File.separator);
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDestFileDefaultPath() {
        return destFileDefaultPath;
    }

    public void setDestFileDefaultPath(String destFileDefaultPath) {
        this.destFileDefaultPath = destFileDefaultPath;
    }

    public String getDestFilePath() {
        return destFilePath;
    }

    public void setDestFilePath(String destFilePath) {
        this.destFilePath = destFilePath;
    }

    public String getModelClassFilePath() {
        return modelClassFilePath;
    }

    public void setModelClassFilePath(String modelClassFilePath) {
        this.modelClassFilePath = modelClassFilePath;
    }

    public String getControllerClassFilePath() {
        return controllerClassFilePath;
    }

    public void setControllerClassFilePath(String controllerClassFilePath) {
        this.controllerClassFilePath = controllerClassFilePath;
    }

    public String getServiceClassFilePath() {
        return serviceClassFilePath;
    }

    public void setServiceClassFilePath(String serviceClassFilePath) {
        this.serviceClassFilePath = serviceClassFilePath;
    }

    public String getMapperClassFilePath() {
        return mapperClassFilePath;
    }

    public void setMapperClassFilePath(String mapperClassFilePath) {
        this.mapperClassFilePath = mapperClassFilePath;
    }

    public String getSrcFolder() {
        return srcFolder;
    }

    public void setSrcFolder(String srcFolder) {
        this.srcFolder = srcFolder;
    }

    public String getModelPackagePath() {
        return modelPackagePath;
    }

    public void setModelPackagePath(String modelPackagePath) {
        this.modelPackagePath = modelPackagePath;
    }

    public String getControllerPackagePath() {
        return controllerPackagePath;
    }

    public void setControllerPackagePath(String controllerPackagePath) {
        this.controllerPackagePath = controllerPackagePath;
    }

    public String getServicePackagePath() {
        return servicePackagePath;
    }

    public void setServicePackagePath(String servicePackagePath) {
        this.servicePackagePath = servicePackagePath;
    }

    public String getMapperPackagePath() {
        return mapperPackagePath;
    }

    public void setMapperPackagePath(String mapperPackagePath) {
        this.mapperPackagePath = mapperPackagePath;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
        this.fileName = fileName(tableName);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getMapper() {
        return mapper;
    }

    public void setMapper(String mapper) {
        this.mapper = mapper;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public boolean isSaveToPackage() {
        return saveToPackage;
    }

    public void setSaveToPackage(boolean saveToPackage) {
        this.saveToPackage = saveToPackage;
    }

    public String fileName(String tableName) {
        tableName = tableName.replaceAll(" ", "");

        if (tableName.equals("_")) {
            return tableName;
        }

        if (tableName.startsWith("_")) {
            tableName = tableName.replaceFirst("_", "");
        }

        if (tableName.startsWith("t_") && tableName.length() > 3) {
            tableName = tableName.replaceFirst("t_", "");
        } else if (tableName.startsWith("T_") && tableName.length() > 3) {
            tableName = tableName.replaceFirst("T_", "");
        }

        String[] names = tableName.split("_");
        String newName = "";
        for (String name : names) {
            if (StringUtils.isNotBlank(name)) {
                newName = newName + com.cas.plugin.util.StringUtils.initcap(name);
            }
        }
//        newName += "Entity";
        return newName;

    }

}

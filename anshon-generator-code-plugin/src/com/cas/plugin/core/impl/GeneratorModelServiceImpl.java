package com.cas.plugin.core.impl;


import com.cas.plugin.core.GeneratorService;
import com.cas.plugin.util.DatasourceUtils;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * @author huang_kangjie
 * @create 2018-09-04 10:57
 **/
public class GeneratorModelServiceImpl implements GeneratorService {

     private static Logger logger = Logger.getLogger(GeneratorModelServiceImpl.class.getCanonicalName());
     @Override
     public void generator() throws FileNotFoundException, UnsupportedEncodingException, SQLException {
          DatasourceUtils.getInstance().generatorModel();
          logger.info("=======================model生成成功！============================");
     }

}

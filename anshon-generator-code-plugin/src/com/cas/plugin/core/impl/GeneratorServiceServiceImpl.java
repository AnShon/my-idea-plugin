package com.cas.plugin.core.impl;


import com.cas.plugin.SettingComponent;
import com.cas.plugin.core.GeneratorService;
import com.cas.plugin.util.TemplateUtils;
import com.cas.plugin.util.StringUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author huang_kangjie
 * @create 2018-09-04 10:57
 **/
public class GeneratorServiceServiceImpl implements GeneratorService {

    private static Logger logger = Logger.getLogger(GeneratorServiceServiceImpl.class.getCanonicalName());

    @Override
    public void generator() throws Exception {
        Writer out = null;

        SettingComponent settingComponent = SettingComponent.getInstance();
        // step1 创建freeMarker配置实例
        Configuration configuration = new Configuration();
        // step2 获取模版路径
        File file = new File(TemplateUtils.templatePath());
        configuration.setDirectoryForTemplateLoading(file);
        // step3 创建数据模型
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = settingComponent.getFileName();
        dataMap.put("fileName", fileName);
        dataMap.put("mapper", fileName.substring(0, 1).toLowerCase() + fileName.substring(1, fileName.length()));
        dataMap.put("mapperClassPath", settingComponent.getMapperPackagePath());
        dataMap.put("serviceClassPath", settingComponent.getServicePackagePath());
        if (org.apache.commons.lang3.StringUtils.isNotBlank(settingComponent.getServicePackagePath())) {
            dataMap.put("servicePackage", "package " + settingComponent.getServicePackagePath());
        } else {
            dataMap.put("servicePackage", " ");
        }
        dataMap.put("auther", settingComponent.getAuther());
        dataMap.put("time", StringUtils.getDate());
        // step4 加载模版文件
        Template template = configuration.getTemplate(TemplateUtils.TEMPLATE_FILE_NAME_SERVICE);
        // step5 生成数据

        File destFile = new File(settingComponent.isSaveToPackage()
                ? settingComponent.getDestFilePath() + File.separator + settingComponent.getServiceClassFilePath() : settingComponent.getDestFilePath());
        if (!destFile.exists()) {
            destFile.mkdirs();
        }
        File docFile = new File(destFile + File.separator + settingComponent.getFileName() + "Service.java");
        if (!docFile.exists()) {
            docFile.createNewFile();
        }
        out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(docFile)));
        // step6 输出文件
        template.process(dataMap, out);
        logger.info("=======================service生成成功！============================");
    }

}

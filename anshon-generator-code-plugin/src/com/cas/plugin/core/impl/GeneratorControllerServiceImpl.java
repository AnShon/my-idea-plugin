package com.cas.plugin.core.impl;

import com.cas.plugin.SettingComponent;
import com.cas.plugin.core.GeneratorService;
import com.cas.plugin.util.TemplateUtils;
import com.cas.plugin.util.StringUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.fest.util.Closeables;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author huang_kangjie
 * @create 2018-09-04 10:57
 **/
public class GeneratorControllerServiceImpl implements GeneratorService {
    private static Logger logger = Logger.getLogger(GeneratorControllerServiceImpl.class.getCanonicalName());

    @Override
    public void generator() throws IOException, TemplateException {
        Writer out = null;
        SettingComponent settingComponent = SettingComponent.getInstance();
        // step1 创建freeMarker配置实例
        Configuration configuration = new Configuration();
        // step2 获取模版路径
        File file = new File(TemplateUtils.templatePath());
        configuration.setDirectoryForTemplateLoading(file);
        // step3 创建数据模型
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("fileName", settingComponent.getFileName());
        dataMap.put("service", settingComponent.getFileName().substring(0, 1).toLowerCase() +
                settingComponent.getFileName().substring(1));
        dataMap.put("mapperClassPath", settingComponent.getMapperClassFilePath());
        if (org.apache.commons.lang3.StringUtils.isNotBlank(settingComponent.getControllerPackagePath())) {
            dataMap.put("controllerPackage", "package " + settingComponent.getControllerPackagePath());
        } else {
            dataMap.put("controllerPackage", " ");
        }
        dataMap.put("serviceClassPath", settingComponent.getServicePackagePath());
        dataMap.put("controllerClassPath", settingComponent.getControllerPackagePath());
        dataMap.put("auther", settingComponent.getAuther());
        dataMap.put("time", StringUtils.getDate());
        // step4 加载模版文件
        Template template = configuration.getTemplate(TemplateUtils.TEMPLATE_FILE_NAME_CONTROLLER);
        // step5 生成数据

        File destFile = new File(settingComponent.isSaveToPackage()
                ? settingComponent.getDestFilePath() + File.separator + settingComponent.getControllerClassFilePath() : settingComponent.getDestFilePath());
        if (!destFile.exists()) {
            destFile.mkdirs();
        }
        File docFile = new File(destFile + File.separator + settingComponent.getFileName() + "Controller.java");
        if (!docFile.exists()) {
            docFile.createNewFile();
        }
        out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(docFile)));
        // step6 输出文件
        template.process(dataMap, out);
        logger.info("=======================controller生成成功！============================");
    }

}

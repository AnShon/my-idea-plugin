package com.cas.plugin.core.impl;


import com.cas.plugin.SettingComponent;
import com.cas.plugin.core.GeneratorService;
import com.cas.plugin.util.TemplateUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author huang_kangjie
 * @create 2018-09-04 10:57
 **/
public class GeneratorMapperServiceImpl implements GeneratorService {
    private static Logger logger = Logger.getLogger(GeneratorMapperServiceImpl.class.getCanonicalName());

    @Override
    public void generator() throws IOException, TemplateException {

        Writer out = null;

        SettingComponent settingComponent = SettingComponent.getInstance();
        // step1 创建freeMarker配置实例
        Configuration configuration = new Configuration();
        // step2 获取模版路径
//               String rootJavaPath = new StringBuffer(System.getProperty("user.dir")).append("/").append(param.getSrcFolder()).append("/").toString();
        File file = new File(TemplateUtils.templatePath());
        configuration.setDirectoryForTemplateLoading(file);
        // step3 创建数据模型
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("fileName", settingComponent.getFileName());
        if (StringUtils.isNotBlank(settingComponent.getMapperPackagePath())) {
            dataMap.put("mapperPackage", "package " + settingComponent.getMapperPackagePath());
        } else {
            dataMap.put("mapperPackage", " ");
        }
        dataMap.put("modelClassPath", settingComponent.getModelPackagePath());
        dataMap.put("auther", settingComponent.getAuther());
        dataMap.put("time", com.cas.plugin.util.StringUtils.getDate());
        // step4 加载模版文件
        Template template = configuration.getTemplate(TemplateUtils.TEMPLATE_FILE_NAME_MAPPER);
        // step5 生成数据
        File destFile = new File(settingComponent.isSaveToPackage()
                ? settingComponent.getDestFilePath() + File.separator + settingComponent.getMapperClassFilePath() : settingComponent.getDestFilePath());
        if (!destFile.exists()) {
            destFile.mkdirs();
        }
//               String outputPath = destFile + File.separator + fileName(settingComponent.getTableName()) + ".java";


        File docFile = new File(destFile + File.separator + settingComponent.getFileName() + "Mapper.java");
        if (!docFile.exists()) {
            docFile.createNewFile();
        }
        out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(docFile)));
        // step6 输出文件
        template.process(dataMap, out);
        logger.info("=======================mapper生成成功！============================");
    }

}

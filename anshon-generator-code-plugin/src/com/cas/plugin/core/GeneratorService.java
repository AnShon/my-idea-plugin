package com.cas.plugin.core;

/**
 * 自动生成代码
 * @author huang_kangjie
 * @create 2018-09-04 10:54
 **/
public interface GeneratorService {

     /**
      * 根据模板生成代码
      */
     void generator() throws  Exception;
}

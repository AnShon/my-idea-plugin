package com.cas.plugin;


import com.cas.plugin.core.factroy.GeneratoerFactroy;
import com.cas.plugin.util.DatasourceUtils;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * 一键无痛苦，自动生成 model、service、controller、service
 * <p>
 * 文件生成位置destFilePath
 *
 * @author huang_kangjie
 * @create 2018-09-04 9:36
 **/
public class GeneratorCode {
    public static void main() throws Exception {
        loadDataSource();
        generatorModel();
        generatorMapper();
        generatorService();
        generatorController();
    }
    /**
     * 加载数据源
     */
    public static void loadDataSource() {
        DatasourceUtils.load();
    }

    /**
     * 根据模板生成model
     */
    public static void generatorModel() throws Exception {
        GeneratoerFactroy.getGenerator(GeneratoerFactroy.GeneratoerType.MODEL).generator();
    }

    /**
     * 根据模板生成controller
     */
    public static void generatorController() throws Exception {
        GeneratoerFactroy.getGenerator(GeneratoerFactroy.GeneratoerType.CONCTROLLER).generator();
    }

    /**
     * 根据模板生成service
     */
    public static void generatorService() throws Exception {
        GeneratoerFactroy.getGenerator(GeneratoerFactroy.GeneratoerType.SERVICE).generator();
    }

    /**
     * 根据模板生成mapper
     */
    public static void generatorMapper() throws Exception {
        GeneratoerFactroy.getGenerator(GeneratoerFactroy.GeneratoerType.MAPPER).generator();
    }

}

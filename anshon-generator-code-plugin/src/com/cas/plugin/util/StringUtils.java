package com.cas.plugin.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author huang_kangjie
 * @create 2018-09-04 10:25
 **/
public class StringUtils {

     public static boolean isEmpty(Object str) {
          return str == null || "".equals(str);
     }

     public static String getDate(){
          Date currentTime = new Date();
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
          String dateString = formatter.format(currentTime);
          return dateString;
     }

     /**
      * 功能：将输入字符串的首字母改成大写
      *
      * @param str
      * @return
      */
     public static String initcap(String str) {

          if (!StringUtils.isEmpty(str)) {
               char[] ch = str.toCharArray();
               if (ch[0] >= 'a' && ch[0] <= 'z') {
                    ch[0] = (char) (ch[0] - 32);
               }

               return new String(ch);
          }
          return str;

     }

}

package com.cas.plugin.util;

import com.cas.plugin.common.Template;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * <p>Description: </p>
 *
 * @author chenanshun
 * @date 2018/11/15 16:26
 */
public class TemplateUtils {
    public static final String TEMPLATE_FILE_NAME_CONTROLLER = "template_controllers.ftl";
    public static final String TEMPLATE_FILE_NAME_MAPPER = "template_mapper.ftl";
    public static final String TEMPLATE_FILE_NAME_SERVICE = "template_service.ftl";
    private static Logger logger = Logger.getLogger(TemplateUtils.class.getCanonicalName());
    private static String URL = null;

    // 模板文件保存路径
    public static String templatePath() {
        if (URL != null) {
            return URL;
        } else {
            String url = System.getProperty("java.class.path").split(";")[0];// 获取jar包路径
            url = url.substring(0, url.indexOf("lib"));
            String pattern = "^(/[A-Za-z]:).*";
            if (Pattern.matches(pattern, url)) {
                url = url.replaceFirst("/", "");
            }
            url = url + "plugins" + File.separator + "generator-code";// 插件文件夹
            File docFile = new File(url);
            if (!docFile.exists()) {
                docFile.mkdirs();
            }
            URL = url;
            return url;
        }
    }

    // 初始化模板文件
    public static void initTemplateFile() {
        new Thread(() -> {
            String controllerTemplateFilePath = templatePath() + File.separator + TEMPLATE_FILE_NAME_CONTROLLER;
            String mapperTemplateFilePath = templatePath() + File.separator + TEMPLATE_FILE_NAME_MAPPER;
            String serviceTemplateFilePath = templatePath() + File.separator + TEMPLATE_FILE_NAME_SERVICE;
            try {
                if (!new File(controllerTemplateFilePath).exists())
                    Files.write(Paths.get(controllerTemplateFilePath), Template.CONTROLLER.getBytes());
                if (!new File(mapperTemplateFilePath).exists())
                    Files.write(Paths.get(mapperTemplateFilePath), Template.MAPPER.getBytes());
                if (!new File(serviceTemplateFilePath).exists())
                    Files.write(Paths.get(serviceTemplateFilePath), Template.SERVICE.getBytes());
            } catch (IOException e) {
                logger.warning("初始化模板文件异常\n" + e.getMessage());
            }
        }).start();
    }
}
